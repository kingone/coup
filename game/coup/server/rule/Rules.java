package game.coup.server.rule;

import java.util.ArrayList;
import java.util.Map;

public class Rules {
	private int[] pos;
	private Board board;
	private char player;

	public ArrayList<int[]> getNextMove(String piece, int[] pos, Board board) {
		this.pos = pos;
		this.board = board;
		this.player = piece.charAt(0);
		switch (piece.charAt(1)) {
		case 'x':
			return xeRules();
		case 'm':
			return maRules();
		case 'p':
			return phaoRules();
		case 't':
			char indexT = piece.charAt(2);
			if ((indexT == '0') || (indexT == '1')) {
				return voiUpRules();
			}
			return voiRules();

		case 's':
			char indexS = piece.charAt(2);
			if ((indexS == '0') || (indexS == '1')) {
				return syUpRules();
			}
			return syRules();

		case 'g':
			return generalRules();
		case 'z':
			return totRules();
		}
		return null;
	}

	private ArrayList<int[]> maRules() {
		ArrayList<int[]> moves = new ArrayList();
		int[][] target = { { 1, -2 }, { 2, -1 }, { 2, 1 }, { 1, 2 }, { -1, 2 }, { -2, 1 }, { -2, -1 }, { -1, -2 } };
		int[][] obstacle = { { 0, -1 }, { 1, 0 }, { 1, 0 }, { 0, 1 }, { 0, 1 }, { -1, 0 }, { -1, 0 }, { 0, -1 } };
		for (int i = 0; i < target.length; i++) {
			int[] e = { this.pos[0] + target[i][0], this.pos[1] + target[i][1] };
			int[] f = { this.pos[0] + obstacle[i][0], this.pos[1] + obstacle[i][1] };
			if ((this.board.isInside(e)) && (this.board.isEmpty(f))) {
				if (this.board.isEmpty(e)) {
					moves.add(e);
				} else if (this.board.getPiece(e).color != this.player)
					moves.add(e);
			}
		}
		return moves;
	}

	private ArrayList<int[]> xeRules() {
		ArrayList<int[]> moves = new ArrayList();
		int[] yOffsets = { 1, 2, 3, 4, 5, 6, 7, 8 };
		int[] xOffsets = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		for (int offset : yOffsets) {
			int[] rMove = { this.pos[0], this.pos[1] + offset };
			if (this.board.isEmpty(rMove)) {
				moves.add(rMove);
			} else {
				if ((!this.board.isInside(rMove)) || (this.board.getPiece(rMove).color == this.player))
					break;
				moves.add(rMove);
				break;
			}
		}
		for (int offset : yOffsets) {
			int[] lMove = { this.pos[0], this.pos[1] - offset };
			if (this.board.isEmpty(lMove)) {
				moves.add(lMove);
			} else {
				if ((!this.board.isInside(lMove)) || (this.board.getPiece(lMove).color == this.player))
					break;
				moves.add(lMove);
				break;
			}
		}
		for (int offset : xOffsets) {
			int[] uMove = { this.pos[0] - offset, this.pos[1] };
			if (this.board.isEmpty(uMove)) {
				moves.add(uMove);
			} else {
				if ((!this.board.isInside(uMove)) || (this.board.getPiece(uMove).color == this.player))
					break;
				moves.add(uMove);
				break;
			}
		}
		for (int offset : xOffsets) {
			int[] dMove = { this.pos[0] + offset, this.pos[1] };
			if (this.board.isEmpty(dMove)) {
				moves.add(dMove);
			} else {
				if ((!this.board.isInside(dMove)) || (this.board.getPiece(dMove).color == this.player))
					break;
				moves.add(dMove);
				break;
			}
		}
		return moves;
	}

	private ArrayList<int[]> phaoRules() {
		ArrayList<int[]> moves = new ArrayList();
		int[] yOffsets = { 1, 2, 3, 4, 5, 6, 7, 8 };
		int[] xOffsets = { 1, 2, 3, 4, 5, 6, 7, 8, 9 };
		boolean rr = false;
		boolean ll = false;
		boolean uu = false;
		boolean dd = false;
		for (int offset : yOffsets) {
			int[] rMove = { this.pos[0], this.pos[1] + offset };
			if (!this.board.isInside(rMove))
				break;
			boolean e = this.board.isEmpty(rMove);
			if (!rr) {
				if (e)
					moves.add(rMove);
				else
					rr = true;
			} else if (!e) {
				if (this.board.getPiece(rMove).color == this.player)
					break;
				moves.add(rMove);
				break;
			}
		}

		for (int offset : yOffsets) {
			int[] lMove = { this.pos[0], this.pos[1] - offset };
			if (!this.board.isInside(lMove))
				break;
			boolean e = this.board.isEmpty(lMove);
			if (!ll) {
				if (e)
					moves.add(lMove);
				else
					ll = true;
			} else if (!e) {
				if (this.board.getPiece(lMove).color == this.player)
					break;
				moves.add(lMove);
				break;
			}
		}

		for (int offset : xOffsets) {
			int[] uMove = { this.pos[0] - offset, this.pos[1] };
			if (!this.board.isInside(uMove))
				break;
			boolean e = this.board.isEmpty(uMove);
			if (!uu) {
				if (e)
					moves.add(uMove);
				else
					uu = true;
			} else if (!e) {
				if (this.board.getPiece(uMove).color == this.player)
					break;
				moves.add(uMove);
				break;
			}
		}

		for (int offset : xOffsets) {
			int[] dMove = { this.pos[0] + offset, this.pos[1] };
			if (!this.board.isInside(dMove))
				break;
			boolean e = this.board.isEmpty(dMove);
			if (!dd) {
				if (e)
					moves.add(dMove);
				else
					dd = true;
			} else if (!e) {
				if (this.board.getPiece(dMove).color == this.player)
					break;
				moves.add(dMove);
				break;
			}
		}

		return moves;
	}

	private ArrayList<int[]> voiRules() {
		ArrayList<int[]> moves = new ArrayList();
		int[][] target = { { -2, -2 }, { 2, -2 }, { -2, 2 }, { 2, 2 } };
		int[][] obstacle = { { -1, -1 }, { 1, -1 }, { -1, 1 }, { 1, 1 } };
		for (int i = 0; i < target.length; i++) {
			int[] e = { this.pos[0] + target[i][0], this.pos[1] + target[i][1] };
			int[] f = { this.pos[0] + obstacle[i][0], this.pos[1] + obstacle[i][1] };
			if ((this.board.isInside(e)) && (this.board.isEmpty(f))) {
				if (this.board.isEmpty(e)) {
					moves.add(e);
				} else if (this.board.getPiece(e).color != this.player)
					moves.add(e);
			}
		}
		return moves;
	}

	private ArrayList<int[]> syRules() {
		ArrayList<int[]> moves = new ArrayList();
		int[][] target = { { -1, -1 }, { 1, 1 }, { -1, 1 }, { 1, -1 } };
		for (int[] aTarget : target) {
			int[] e = { this.pos[0] + aTarget[0], this.pos[1] + aTarget[1] };
			if (this.board.isInside(e))
				if (this.board.isEmpty(e)) {
					moves.add(e);
				} else if (this.board.getPiece(e).color != this.player)
					moves.add(e);
		}
		return moves;
	}

	private ArrayList<int[]> voiUpRules() {
		ArrayList<int[]> moves = new ArrayList();
		int[][] target = { { -2, -2 }, { 2, -2 }, { -2, 2 }, { 2, 2 } };
		int[][] obstacle = { { -1, -1 }, { 1, -1 }, { -1, 1 }, { 1, 1 } };
		for (int i = 0; i < target.length; i++) {
			int[] e = { this.pos[0] + target[i][0], this.pos[1] + target[i][1] };
			int[] f = { this.pos[0] + obstacle[i][0], this.pos[1] + obstacle[i][1] };
			if ((this.board.isInside(e)) && ((e[0] <= 4) || (this.player != 'b'))
					&& ((e[0] >= 5) || (this.player != 'r')) && (this.board.isEmpty(f))) {
				if (this.board.isEmpty(e)) {
					moves.add(e);
				} else if (this.board.getPiece(e).color != this.player)
					moves.add(e);
			}
		}
		return moves;
	}

	private ArrayList<int[]> syUpRules() {
		ArrayList<int[]> moves = new ArrayList();
		int[][] target = { { -1, -1 }, { 1, 1 }, { -1, 1 }, { 1, -1 } };
		for (int[] aTarget : target) {
			int[] e = { this.pos[0] + aTarget[0], this.pos[1] + aTarget[1] };
			if ((this.board.isInside(e)) && (((e[0] <= 2) && (e[1] >= 3) && (e[1] <= 5)) || ((this.player != 'b')
					&& (((e[0] >= 7) && (e[1] >= 3) && (e[1] <= 5)) || (this.player != 'r'))))) {
				if (this.board.isEmpty(e)) {
					moves.add(e);
				} else if (this.board.getPiece(e).color != this.player)
					moves.add(e);
			}
		}
		return moves;
	}

	private ArrayList<int[]> generalRules() {
		ArrayList<int[]> moves = new ArrayList();

		int[][] target = { { 0, 1 }, { 0, -1 }, { 1, 0 }, { -1, 0 } };
		for (int[] aTarget : target) {
			int[] e = { this.pos[0] + aTarget[0], this.pos[1] + aTarget[1] };
			if ((this.board.isInside(e)) && (((e[0] <= 2) && (e[1] >= 3) && (e[1] <= 5)) || ((this.player != 'b')
					&& (((e[0] >= 7) && (e[1] >= 3) && (e[1] <= 5)) || (this.player != 'r'))))) {
				if (this.board.isEmpty(e)) {
					moves.add(e);
				} else if (this.board.getPiece(e).color != this.player)
					moves.add(e);
			}
		}
		boolean flag = true;
		int[] oppoBoss = this.player == 'r' ? ((Piece) this.board.pieces.get("bg0")).position
				: ((Piece) this.board.pieces.get("rg0")).position;
		if (oppoBoss[1] == this.pos[1]) {
			for (int i = Math.min(oppoBoss[0], this.pos[0]) + 1; i < Math.max(oppoBoss[0], this.pos[0]); i++) {
				if (this.board.getPiece(i, this.pos[1]) != null) {
					flag = false;
					break;
				}
			}
			if (flag)
				moves.add(oppoBoss);
		}
		return moves;
	}

	private ArrayList<int[]> totRules() {
		ArrayList<int[]> moves = new ArrayList();
		int[][] targetU = { { 0, 1 }, { 0, -1 }, { -1, 0 } };
		int[][] targetD = { { 0, 1 }, { 0, -1 }, { 1, 0 } };
		if (this.player == 'r') {
			if (this.pos[0] > 4) {
				int[] e = new int[] { this.pos[0] - 1, this.pos[1] };
				if (this.board.isEmpty(e)) {
					moves.add(e);
				} else if (this.board.getPiece(e).color != this.player)
					moves.add(e);
			} else {
				for (int[] aTarget : targetU) {
					int[] e = { this.pos[0] + aTarget[0], this.pos[1] + aTarget[1] };
					if (this.board.isInside(e))
						if (this.board.isEmpty(e)) {
							moves.add(e);
						} else if (this.board.getPiece(e).color != this.player)
							moves.add(e);
				}
			}
		}
		if (this.player == 'b') {
			if (this.pos[0] < 5) {
				int[] e = new int[] { this.pos[0] + 1, this.pos[1] };
				if (this.board.isEmpty(e)) {
					moves.add(e);
				} else if (this.board.getPiece(e).color != this.player)
					moves.add(e);
			} else {
				for (int[] aTarget : targetD) {
					int[] e = { this.pos[0] + aTarget[0], this.pos[1] + aTarget[1] };
					if (this.board.isInside(e)) {
						if (this.board.isEmpty(e)) {
							moves.add(e);
						} else if (this.board.getPiece(e).color != this.player)
							moves.add(e);
					}
				}
			}
		}
		return moves;
	}
}
