/*     */ package game.coup.server.rule;
/*     */ 
/*     */ import game.coup.server.rule.ai.Move;
/*     */ import java.util.ArrayList;
/*     */ import java.util.Collection;
/*     */ import java.util.Collections;
/*     */ import java.util.HashMap;
/*     */ import java.util.LinkedList;
/*     */ import java.util.List;
/*     */ import java.util.Map;
/*     */ import java.util.Map.Entry;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Board
/*     */ {
/*     */   public static final int BOARD_WIDTH = 9;
/*     */   public static final int BOARD_HEIGHT = 10;
/*  21 */   public Map<String, Piece> pieces = new HashMap();
/*     */   
/*  23 */   private List<Piece> redRandomPieces = new LinkedList();
/*  24 */   private int redIndex = -1;
/*  25 */   private List<Piece> blackRandomPieces = new LinkedList();
/*  26 */   private int blackIndex = -1;
/*     */   
/*     */   public void initRandomPiece() {
/*  29 */     Collection<Piece> allPieces = this.pieces.values();
/*  30 */     for (Piece piece : allPieces) {
/*  31 */       Piece p = new Piece(piece);
/*  32 */       if ((p.color == 'r') && 
/*  33 */         (p.character != 'g')) {
/*  34 */         this.redRandomPieces.add(p);
/*     */       }
/*     */       
/*  37 */       if ((p.color == 'b') && 
/*  38 */         (p.character != 'g')) {
/*  39 */         this.blackRandomPieces.add(p);
/*     */       }
/*     */     }
/*     */     
/*  43 */     Collections.shuffle(this.redRandomPieces);
/*  44 */     Collections.shuffle(this.blackRandomPieces);
/*     */   }
/*     */   
/*     */   public Piece getRandomPieces(char color) {
/*  48 */     Piece p = null;
/*  49 */     if (color == 'r') {
/*  50 */       if (this.redIndex >= this.redRandomPieces.size()) {
/*  51 */         this.redIndex = (this.redRandomPieces.size() - 1);
/*     */       }
/*  53 */       p = (Piece)this.redRandomPieces.get(++this.redIndex);
/*     */     }
/*  55 */     if (color == 'b') {
/*  56 */       if (this.blackIndex >= this.blackRandomPieces.size()) {
/*  57 */         this.blackIndex = (this.blackRandomPieces.size() - 1);
/*     */       }
/*  59 */       p = (Piece)this.blackRandomPieces.get(++this.blackIndex);
/*     */     }
/*  61 */     return p;
/*     */   }
/*     */   
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*  69 */   public Map<String, Piece> deadPieces = new HashMap();
/*  70 */   public List<Move> moveList = new LinkedList();
/*  71 */   public char player = 'b';
/*  72 */   public Piece[][] cells = new Piece[10][9];
/*  73 */   public int stateCount = 0;
/*     */   
/*  75 */   public void reset() { this.redRandomPieces.clear();
/*  76 */     this.blackRandomPieces.clear();
/*  77 */     this.redIndex = -1;
/*  78 */     this.blackIndex = -1;
/*     */     
/*  80 */     this.pieces.clear();
/*  81 */     this.deadPieces.clear();
/*  82 */     this.moveList.clear();
/*  83 */     this.player = 'b';
/*  84 */     for (int i = 0; i < 10; i++) {
/*  85 */       for (int j = 0; j < 9; j++) {
/*  86 */         this.cells[i][j] = null;
/*     */       }
/*     */     }
/*  89 */     this.stateCount = 0;
/*     */   }
/*     */   
/*  92 */   public boolean isInside(int[] position) { return isInside(position[0], position[1]); }
/*     */   
/*     */   public boolean isInside(int x, int y)
/*     */   {
/*  96 */     return (x >= 0) && (x < 10) && (y >= 0) && (y < 9);
/*     */   }
/*     */   
/*     */   public boolean isEmpty(int[] position)
/*     */   {
/* 101 */     return isEmpty(position[0], position[1]);
/*     */   }
/*     */   
/*     */   public boolean isEmpty(int x, int y) {
/* 105 */     return (isInside(x, y)) && (this.cells[x][y] == null);
/*     */   }
/*     */   
/*     */   public boolean update(Piece piece)
/*     */   {
/* 110 */     int[] pos = piece.position;
/* 111 */     this.cells[pos[0]][pos[1]] = piece;
/* 112 */     return true;
/*     */   }
/*     */   
/*     */   public Piece updatePiece(int[] oldPos, int[] newPos) {
/* 116 */     Piece orig = getPiece(oldPos);
/* 117 */     Piece inNewPos = getPiece(newPos);
/*     */     
/* 119 */     if (inNewPos != null) {
/* 120 */       if (inNewPos.isTrans) {
/* 121 */         this.pieces.remove(inNewPos.transKey);
/* 122 */         this.deadPieces.put(inNewPos.transKey, inNewPos);
/*     */       } else {
/* 124 */         this.pieces.remove(inNewPos.key);
/* 125 */         inNewPos.deadBeforeTrans = true;
/* 126 */         this.deadPieces.put(inNewPos.key, inNewPos);
/* 127 */         inNewPos.transform();
/*     */       }
/*     */     }
/*     */     
/* 131 */     int[] origPos = orig.position;
/* 132 */     this.cells[origPos[0]][origPos[1]] = null;
/* 133 */     this.cells[newPos[0]][newPos[1]] = orig;
/* 134 */     orig.position = newPos;
/* 135 */     this.player = (this.player == 'r' ? 'b' : 'r');
/* 136 */     return inNewPos;
/*     */   }
/*     */   
/*     */   public boolean backPiece(String key) {
/* 140 */     int[] origPos = ((Piece)this.pieces.get(key)).position;
/* 141 */     this.cells[origPos[0]][origPos[1]] = ((Piece)this.pieces.get(key));
/* 142 */     return true;
/*     */   }
/*     */   
/*     */   public Piece getPiece(int[] pos) {
/* 146 */     return getPiece(pos[0], pos[1]);
/*     */   }
/*     */   
/*     */ 
/* 150 */   public Piece getPiece(int x, int y) { return this.cells[x][y]; }
/*     */   
/*     */   public Board clone() {
/* 153 */     Board b = new Board();
/* 154 */     b.player = this.player;
/* 155 */     b.pieces = new HashMap();
/* 156 */     for (Map.Entry<String, Piece> entry : this.pieces.entrySet()) {
/* 157 */       Piece piece = (Piece)entry.getValue();
/* 158 */       b.pieces.put(piece.key, piece);
/* 159 */       b.cells[piece.position[0]][piece.position[1]] = piece;
/*     */     }
/* 161 */     for (Map.Entry<String, Piece> entry : this.deadPieces.entrySet()) {
/* 162 */       Piece piece = (Piece)entry.getValue();
/* 163 */       b.deadPieces.put(piece.key, piece);
/*     */     }
/* 165 */     return b;
/*     */   }
/*     */   
/*     */   public void revert(Move move) {
/* 169 */     this.cells[move.from[0]][move.from[1]] = move.piece;
/* 170 */     move.piece.position = move.from;
/* 171 */     if (move.isTrans) {
/* 172 */       move.piece.transback();
/*     */     }
/* 174 */     if (move.eatenPiece != null) {
/* 175 */       this.cells[move.to[0]][move.to[1]] = move.eatenPiece;
/* 176 */       if (move.eatenPiece.deadBeforeTrans) {
/* 177 */         move.eatenPiece.transback();
/* 178 */         move.eatenPiece.deadBeforeTrans = false;
/*     */       }
/* 180 */       if (move.eatenPiece.isTrans) {
/* 181 */         this.pieces.put(move.eatenPiece.transKey, move.eatenPiece);
/* 182 */         this.deadPieces.remove(move.eatenPiece.transKey);
/*     */       } else {
/* 184 */         this.pieces.put(move.eatenPiece.key, move.eatenPiece);
/* 185 */         this.deadPieces.remove(move.eatenPiece.key);
/*     */       }
/*     */     } else {
/* 188 */       this.cells[move.to[0]][move.to[1]] = null;
/*     */     }
/* 190 */     if (this.player == 'r') {
/* 191 */       this.player = 'b';
/*     */     } else {
/* 193 */       this.player = 'r';
/*     */     }
/*     */   }
/*     */   
/*     */   public byte[][] getMap() {
/* 198 */     byte[][] map = new byte[10][9];
/* 199 */     for (int i = 0; i < 10; i++) {
/* 200 */       for (int j = 0; j < 9; j++) {
/* 201 */         Piece p = this.cells[i][j];
/* 202 */         map[i][j] = ((byte)p.character);
/*     */       }
/*     */     }
/* 205 */     return new byte[0][0];
/*     */   }
/*     */   
/* 208 */   public String[][] getMapKey() { String[][] map = new String[10][9];
/* 209 */     for (int i = 0; i < 10; i++) {
/* 210 */       for (int j = 0; j < 9; j++) {
/* 211 */         Piece p = this.cells[i][j];
/* 212 */         if (p != null) {
/* 213 */           map[i][j] = p.key;
/*     */         } else {
/* 215 */           map[i][j] = "";
/*     */         }
/*     */       }
/*     */     }
/* 219 */     return map;
/*     */   }
/*     */   
/* 222 */   public byte[][] getMapStatus() { byte[][] map = new byte[10][9];
/* 223 */     for (int i = 0; i < 10; i++) {
/* 224 */       for (int j = 0; j < 9; j++) {
/* 225 */         Piece p = this.cells[i][j];
/* 226 */         if (p != null) {
/* 227 */           map[i][j] = ((byte)(p.isTrans ? 2 : 1));
/*     */         } else {
/* 229 */           map[i][j] = 0;
/*     */         }
/*     */       }
/*     */     }
/* 233 */     return map; }
/*     */   
/* 235 */   public int blackMate = 0;
/* 236 */   public int redMate = 0;
/*     */   
/* 238 */   public int checkLoopMove(Move move) { int size = this.moveList.size();
/* 239 */     int rangeCheck = 12;
/* 240 */     if (size < rangeCheck) {
/* 241 */       return 0;
/*     */     }
/* 243 */     int min = size - rangeCheck;
/* 244 */     int c = 0;
/* 245 */     for (int i = size - 1; i >= min; i--) {
/* 246 */       Move m = (Move)this.moveList.get(i);
/* 247 */       if (m.sameWith(move)) {
/* 248 */         c++;
/*     */       }
/*     */     }
/* 251 */     if (c >= 3) {
/* 252 */       return 5;
/*     */     }
/*     */     
/* 255 */     return 0;
/*     */   }
/*     */   
/*     */   public Move getLastMove() {
/* 259 */     int size = this.moveList.size();
/* 260 */     if (size > 0) {
/* 261 */       return (Move)this.moveList.get(size - 1);
/*     */     }
/* 263 */     return null;
/*     */   }
/*     */   
/*     */   public Move getPreviousMove(Move move) {
/* 267 */     int size = this.moveList.size();
/* 268 */     for (int i = size - 1; i >= 0; i--) {
/* 269 */       Move m = (Move)this.moveList.get(i);
/* 270 */       if (m.piece.color == move.piece.color) {
/* 271 */         return m;
/*     */       }
/*     */     }
/* 274 */     return null;
/*     */   }
/*     */   
/*     */   public String[] getDeadPieces(char color) {
/* 278 */     List<Piece> halfDie = new ArrayList();
/* 279 */     for (Map.Entry<String, Piece> entry : this.deadPieces.entrySet()) {
/* 280 */       Piece p = (Piece)entry.getValue();
/* 281 */       if (p.color != color) {
/* 282 */         halfDie.add(p);
/*     */       }
/*     */     }
/* 285 */     String[] res = new String[halfDie.size()];
/* 286 */     for (int i = 0; i < halfDie.size(); i++) {
/* 287 */       res[i] = ((Piece)halfDie.get(i)).key;
/*     */     }
/* 289 */     return res;
/*     */   }
/*     */   
/*     */   Piece updatePiece(int[] oldPos, int[] newPos, Move mv) {
/* 293 */     Piece orig = getPiece(oldPos);
/* 294 */     Piece inNewPos = getPiece(newPos);
/*     */     
/* 296 */     if (inNewPos != null) {
/* 297 */       if (inNewPos.isTrans) {
/* 298 */         inNewPos.transKey = mv.eatenPiece.transKey;
/* 299 */         inNewPos.key = mv.eatenPiece.key;
/* 300 */         this.pieces.remove(inNewPos.transKey);
/* 301 */         this.deadPieces.put(inNewPos.transKey, inNewPos);
/*     */       } else {
/* 303 */         inNewPos.transKey = mv.eatenPiece.key;
/* 304 */         inNewPos.key = mv.eatenPiece.transKey;
/* 305 */         this.pieces.remove(inNewPos.key);
/* 306 */         inNewPos.deadBeforeTrans = true;
/* 307 */         this.deadPieces.put(inNewPos.key, inNewPos);
/* 308 */         inNewPos.transform();
/*     */       }
/*     */     }
/*     */     
/* 312 */     int[] origPos = orig.position;
/* 313 */     this.cells[origPos[0]][origPos[1]] = null;
/* 314 */     this.cells[newPos[0]][newPos[1]] = orig;
/* 315 */     orig.position = newPos;
/* 316 */     this.player = (this.player == 'r' ? 'b' : 'r');
/* 317 */     return inNewPos;
/*     */   }
/*     */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\rule\Board.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */