/*     */ package game.coup.server.rule;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class Piece
/*     */   implements Cloneable
/*     */ {
/*   9 */   public String key = "";
/*     */   public char color;
/*     */   public char character;
/*     */   public char index;
/*  13 */   public int[] position = new int[2];
/*     */   
/*     */ 
/*  16 */   public String transKey = "";
/*  17 */   public boolean isTrans = false;
/*  18 */   public boolean deadBeforeTrans = false;
/*     */   
/*     */   public String toString() {
/*  21 */     StringBuilder sb = new StringBuilder();
/*  22 */     sb.append("|key:").append(this.key);
/*  23 */     sb.append("|transKey:").append(this.transKey);
/*  24 */     sb.append("|isTrans:").append(this.isTrans);
/*  25 */     sb.append("|deadBeforeTrans:").append(this.deadBeforeTrans);
/*  26 */     return sb.toString();
/*     */   }
/*     */   
/*     */   public void transform() {
/*  30 */     if (!this.isTrans) {
/*  31 */       String tmp = this.key;
/*  32 */       this.key = this.transKey;
/*  33 */       this.color = this.key.charAt(0);
/*  34 */       this.character = this.key.charAt(1);
/*  35 */       this.index = this.key.charAt(2);
/*  36 */       this.isTrans = true;
/*  37 */       this.transKey = tmp;
/*     */     }
/*     */   }
/*     */   
/*     */   public void transback() {
/*  42 */     if (this.isTrans) {
/*  43 */       if ((this.transKey.equals("rz2")) || (this.key.equals("rz2"))) {
/*  44 */         int i = 0;
/*     */       }
/*  46 */       String tmp = this.key;
/*  47 */       this.key = this.transKey;
/*  48 */       this.color = this.key.charAt(0);
/*  49 */       this.character = this.key.charAt(1);
/*  50 */       this.index = this.key.charAt(2);
/*  51 */       this.isTrans = false;
/*  52 */       this.transKey = tmp;
/*     */     }
/*     */   }
/*     */   
/*     */   public Piece(String name, int[] position)
/*     */   {
/*  58 */     this.key = name;
/*  59 */     this.color = name.charAt(0);
/*  60 */     this.character = name.charAt(1);
/*  61 */     this.index = name.charAt(2);
/*  62 */     this.position = position;
/*     */   }
/*     */   
/*     */   public Piece(Piece piece) {
/*  66 */     this.color = piece.color;
/*  67 */     this.character = piece.character;
/*  68 */     if (piece.index == '0') {
/*  69 */       if (this.character == 'z') {
/*  70 */         this.index = '5';
/*     */       } else {
/*  72 */         this.index = '2';
/*     */       }
/*     */     }
/*  75 */     if (piece.index == '1') {
/*  76 */       if (this.character == 'z') {
/*  77 */         this.index = '6';
/*     */       } else {
/*  79 */         this.index = '3';
/*     */       }
/*     */     }
/*  82 */     if (piece.index == '2') {
/*  83 */       this.index = '7';
/*     */     }
/*  85 */     if (piece.index == '3') {
/*  86 */       this.index = '8';
/*     */     }
/*  88 */     if (piece.index == '4') {
/*  89 */       this.index = '9';
/*     */     }
/*  91 */     StringBuilder sb = new StringBuilder();
/*  92 */     sb.append(this.color).append(this.character).append(this.index);
/*  93 */     this.key = sb.toString();
/*  94 */     this.position[0] = piece.position[0];
/*  95 */     this.position[1] = piece.position[1];
/*  96 */     this.isTrans = piece.isTrans;
/*     */   }
/*     */   
/*     */ 
/* 100 */   public int hashCode() { return this.key.hashCode(); }
/*     */   
/*     */   public boolean equals(Object o) {
/* 103 */     if ((o instanceof Piece)) {
/* 104 */       Piece p = (Piece)o;
/* 105 */       if (p.key.equalsIgnoreCase(this.key)) {
/* 106 */         return true;
/*     */       }
/* 108 */       return false;
/*     */     }
/*     */     
/* 111 */     return false;
/*     */   }
/*     */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\rule\Piece.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */