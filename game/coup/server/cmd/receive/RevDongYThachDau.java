/*    */ package game.coup.server.cmd.receive;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseCmd;
/*    */ import bitzero.server.extensions.data.DataCmd;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ public class RevDongYThachDau
/*    */   extends BaseCmd
/*    */ {
/* 10 */   public boolean dongYThachDau = false;
/*    */   public String enemy;
/*    */   public int moneyBet;
/*    */   
/* 14 */   public RevDongYThachDau(DataCmd dataCmd) { super(dataCmd);
/* 15 */     unpackData();
/*    */   }
/*    */   
/*    */   public void unpackData()
/*    */   {
/* 20 */     ByteBuffer bf = makeBuffer();
/* 21 */     this.dongYThachDau = readBoolean(bf);
/* 22 */     this.enemy = readString(bf);
/* 23 */     this.moneyBet = readInt(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\cmd\receive\RevDongYThachDau.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */