/*    */ package game.coup.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import game.coup.server.GamePlayer;
/*    */ import game.coup.server.sPlayerInfo;
/*    */ import game.entities.PlayerInfo;
/*    */ import game.modules.gameRoom.entities.GameMoneyInfo;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SendReconnectInfo
/*    */   extends BaseMsg
/*    */ {
/*    */   public int maxUserPerRoom;
/*    */   public byte chair;
/*    */   public String[][] map;
/*    */   public byte[][] statusMap;
/*    */   public int gameState;
/*    */   public int gameAction;
/*    */   public int currentChair;
/*    */   public int countdownTime;
/*    */   public int moneyType;
/*    */   public long roomBet;
/*    */   public int gameId;
/*    */   public int roomId;
/* 31 */   public byte[] lastMove = new byte[2];
/*    */   
/*    */   public String[] diePieces;
/* 34 */   public boolean[] hasInfoAtChair = new boolean[20];
/*    */   
/* 36 */   public GamePlayer[] pInfos = new GamePlayer[20];
/*    */   
/*    */   public SendReconnectInfo() {
/* 39 */     super((short)3124);
/*    */   }
/*    */   
/*    */   public byte[] createData() {
/* 43 */     ByteBuffer bf = makeBuffer();
/*    */     
/* 45 */     bf.put((byte)this.maxUserPerRoom);
/* 46 */     bf.put(this.chair);
/* 47 */     bf.putShort((short)this.map.length);
/* 48 */     for (int i = 0; i < this.map.length; i++) {
/* 49 */       putStringArray(bf, this.map[i]);
/*    */     }
/* 51 */     bf.put((byte)this.gameState);
/* 52 */     bf.put((byte)this.gameAction);
/* 53 */     bf.put((byte)this.countdownTime);
/* 54 */     bf.put((byte)this.currentChair);
/* 55 */     bf.put((byte)this.moneyType);
/* 56 */     putLong(bf, this.roomBet);
/* 57 */     bf.putInt(this.gameId);
/* 58 */     bf.putInt(this.roomId);
/*    */     
/* 60 */     putBooleanArray(bf, this.hasInfoAtChair);
/* 61 */     for (int i = 0; i < 20; i++) {
/* 62 */       if (this.hasInfoAtChair[i]) {
/* 63 */         GamePlayer gp = this.pInfos[i];
/*    */         
/* 65 */         bf.put((byte)gp.playerStatus);
/* 66 */         PlayerInfo pInfo = gp.pInfo;
/* 67 */         putStr(bf, pInfo.avatarUrl);
/* 68 */         putStr(bf, pInfo.nickName);
/* 69 */         if (gp.gameMoneyInfo != null) {
/* 70 */           putLong(bf, gp.gameMoneyInfo.currentMoney);
/*    */         } else {
/* 72 */           putLong(bf, 0L);
/*    */         }
/* 74 */         bf.put((byte)gp.gameChair);
/* 75 */         bf.put((byte)gp.spInfo.chessColor);
/* 76 */         bf.putInt(gp.spInfo.gameTime);
/*    */       }
/*    */     }
/* 79 */     putByteArray(bf, this.lastMove);
/* 80 */     bf.putShort((short)this.statusMap.length);
/* 81 */     for (int i = 0; i < this.statusMap.length; i++) {
/* 82 */       putByteArray(bf, this.statusMap[i]);
/*    */     }
/* 84 */     putStringArray(bf, this.diePieces);
/* 85 */     return packBuffer(bf);
/*    */   }
/*    */   
/*    */   public void initPrivateInfo(GamePlayer gp) {
/* 89 */     this.chair = ((byte)gp.chair);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\cmd\send\SendReconnectInfo.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */