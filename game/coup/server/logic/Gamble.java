/*    */ package game.coup.server.logic;
/*    */ 
/*    */ import game.coup.server.rule.Board;
/*    */ import game.coup.server.rule.GameController;
/*    */ import game.modules.gameRoom.entities.GameRoomIdGenerator;
/*    */ 
/*    */ 
/*    */ public class Gamble
/*    */ {
/* 10 */   public CardSuit suit = new CardSuit();
/*    */   
/* 12 */   public GameController controller = new GameController();
/* 13 */   public Board board = new Board();
/*    */   public int id;
/* 15 */   public boolean isCheat = false;
/*    */   public long logTime;
/*    */   
/*    */   private static int getID() {
/* 19 */     int id = GameRoomIdGenerator.instance().getId();
/* 20 */     return id;
/*    */   }
/*    */   
/* 23 */   public Gamble() { reset(); }
/*    */   
/*    */   public void reset()
/*    */   {
/* 27 */     this.id = getID();
/* 28 */     this.logTime = System.currentTimeMillis();
/* 29 */     this.board.reset();
/* 30 */     this.controller.initBoard(this.board);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\logic\Gamble.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */