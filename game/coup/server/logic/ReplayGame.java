/*    */ package game.coup.server.logic;
/*    */ 
/*    */ import game.coup.server.rule.Piece;
/*    */ import game.coup.server.rule.ai.Move;
/*    */ import java.io.BufferedReader;
/*    */ import java.io.File;
/*    */ import java.io.FileInputStream;
/*    */ import java.io.InputStreamReader;
/*    */ import java.io.PrintStream;
/*    */ import java.io.Reader;
/*    */ import java.util.LinkedList;
/*    */ import java.util.List;
/*    */ 
/*    */ public class ReplayGame
/*    */ {
/* 16 */   public int startChair = 0;
/*    */   public List<Move> moves;
/* 18 */   public int index = 0;
/*    */   
/* 20 */   public static void main(String[] args) { ReplayGame replayer = new ReplayGame(); }
/*    */   
/*    */   public ReplayGame() {
/* 23 */     this.moves = parseMoves();
/* 24 */     if (this.moves.size() > 0) {
/* 25 */       Move firstMove = (Move)this.moves.get(0);
/* 26 */       this.startChair = firstMove.currentChair;
/*    */     }
/*    */   }
/*    */   
/* 30 */   public Move getMove() { if (this.index == this.moves.size()) {
/* 31 */       this.index = 0;
/*    */     }
/* 33 */     return (Move)this.moves.get(this.index++);
/*    */   }
/*    */   
/* 36 */   public List<Move> parseMoves() { List<Move> moves = new LinkedList();
/* 37 */     String log = loadGameLog();
/* 38 */     String[] lines = log.split(">");
/* 39 */     for (int i = 0; i < lines.length; i++) {
/* 40 */       String line = lines[i];
/* 41 */       String[] lineLog = line.split("<");
/* 42 */       if (lineLog.length == 2) {
/* 43 */         String action = lineLog[0];
/* 44 */         String detail = lineLog[1];
/* 45 */         if (action.contains("DQ")) {
/* 46 */           Move move = parseMove(detail);
/* 47 */           moves.add(move);
/*    */         }
/*    */       }
/*    */     }
/* 51 */     return moves;
/*    */   }
/*    */   
/*    */   public Move parseMove(String detailLog) {
/* 55 */     Move move = new Move();
/*    */     try {
/* 57 */       String[] infos = detailLog.split(";");
/* 58 */       move.currentChair = Integer.parseInt(infos[0]);
/*    */       
/* 60 */       String[] pp = infos[1].split("/");
/* 61 */       move.from = new int[2];
/* 62 */       move.from[0] = Integer.parseInt(infos[2]);
/* 63 */       move.from[1] = Integer.parseInt(infos[3]);
/* 64 */       Piece p = new Piece(pp[0], move.from);
/* 65 */       p.transKey = pp[1];
/* 66 */       move.piece = p;
/*    */       
/* 68 */       move.to = new int[2];
/* 69 */       move.to[0] = Integer.parseInt(infos[4]);
/* 70 */       move.to[1] = Integer.parseInt(infos[5]);
/* 71 */       if (infos.length == 8) {
/* 72 */         String[] ep = infos[6].split("/");
/* 73 */         Piece e = new Piece(ep[0], move.from);
/* 74 */         e.transKey = ep[1];
/* 75 */         move.eatenPiece = e;
/*    */       }
/*    */     } catch (Exception e) {
/* 78 */       System.err.println(detailLog);
/*    */     }
/* 80 */     return move;
/*    */   }
/*    */   
/*    */   public String loadGameLog() {
/* 84 */     String path = System.getProperty("user.dir");
/* 85 */     File file = new File(path + "/conf/gameLog.txt");
/* 86 */     StringBuffer contents = new StringBuffer();
/* 87 */     BufferedReader reader = null;
/*    */     try {
/* 89 */       Reader r = new InputStreamReader(new FileInputStream(file), "UTF-8");
/* 90 */       reader = new BufferedReader(r);
/* 91 */       String text = null;
/* 92 */       while ((text = reader.readLine()) != null) {
/* 93 */         contents.append(text).append(System.getProperty("line.separator"));
/*    */       }
/* 95 */       return contents.toString();
/*    */     } catch (Exception e) {
/* 97 */       e.printStackTrace();
/*    */     }
/* 99 */     return "";
/*    */   }
/*    */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\logic\ReplayGame.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */