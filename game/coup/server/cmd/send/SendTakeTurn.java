/*    */ package game.coup.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SendTakeTurn
/*    */   extends BaseMsg
/*    */ {
/* 13 */   public int chair = 20;
/*    */   public byte[] from;
/*    */   public byte[] to;
/*    */   public String key;
/* 17 */   public String die = "none";
/*    */   
/*    */   public int result;
/* 20 */   public boolean isTrans = false;
/* 21 */   public String newKey = "";
/*    */   
/* 23 */   public SendTakeTurn() { super((short)3101); }
/*    */   
/*    */   public byte[] createData()
/*    */   {
/* 27 */     ByteBuffer bf = makeBuffer();
/* 28 */     bf.put((byte)this.chair);
/* 29 */     putByteArray(bf, this.from);
/* 30 */     putByteArray(bf, this.to);
/* 31 */     putStr(bf, this.key);
/* 32 */     putStr(bf, this.die);
/* 33 */     bf.put((byte)this.result);
/* 34 */     putBoolean(bf, Boolean.valueOf(this.isTrans));
/* 35 */     putStr(bf, this.newKey);
/* 36 */     return packBuffer(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\cmd\send\SendTakeTurn.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */