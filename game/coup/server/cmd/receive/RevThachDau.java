/*    */ package game.coup.server.cmd.receive;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseCmd;
/*    */ 
/*    */ public class RevThachDau extends BaseCmd
/*    */ {
/*    */   public String enemy;
/*    */   public int moneyBet;
/*    */   
/*    */   public RevThachDau(bitzero.server.extensions.data.DataCmd dataCmd)
/*    */   {
/* 12 */     super(dataCmd);
/* 13 */     unpackData();
/*    */   }
/*    */   
/*    */   public void unpackData()
/*    */   {
/* 18 */     java.nio.ByteBuffer bf = makeBuffer();
/* 19 */     this.enemy = readString(bf);
/* 20 */     this.moneyBet = readInt(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\cmd\receive\RevThachDau.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */