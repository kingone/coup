/*    */ package game.coup.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ 
/*    */ public class SendDeadPiece
/*    */   extends BaseMsg
/*    */ {
/* 10 */   public String key = "x";
/*    */   
/* 12 */   public SendDeadPiece() { super((short)3126); }
/*    */   
/*    */   public byte[] createData()
/*    */   {
/* 16 */     ByteBuffer bf = makeBuffer();
/* 17 */     putStr(bf, this.key);
/* 18 */     return packBuffer(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\cmd\send\SendDeadPiece.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */