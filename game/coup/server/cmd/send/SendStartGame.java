/*    */ package game.coup.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import game.coup.server.GamePlayer;
/*    */ import game.coup.server.sPlayerInfo;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SendStartGame
/*    */   extends BaseMsg
/*    */ {
/*    */   public int starter;
/* 14 */   public boolean[] hasInfoAtChair = new boolean[20];
/* 15 */   public GamePlayer[] gamePlayers = new GamePlayer[20];
/*    */   
/* 17 */   public SendStartGame() { super((short)3100); }
/*    */   
/*    */   public byte[] createData()
/*    */   {
/* 21 */     ByteBuffer bf = makeBuffer();
/* 22 */     bf.put((byte)this.starter);
/* 23 */     putBooleanArray(bf, this.hasInfoAtChair);
/* 24 */     for (int i = 0; i < 20; i++) {
/* 25 */       if (this.hasInfoAtChair[i]) {
/* 26 */         bf.put((byte)this.gamePlayers[i].gameChair);
/* 27 */         bf.put((byte)this.gamePlayers[i].playerStatus);
/* 28 */         bf.put((byte)this.gamePlayers[i].spInfo.chessColor);
/* 29 */         bf.putInt(this.gamePlayers[i].spInfo.turnTime);
/* 30 */         bf.putInt(this.gamePlayers[i].spInfo.gameTime);
/*    */       }
/*    */     }
/* 33 */     return packBuffer(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\cmd\send\SendStartGame.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */