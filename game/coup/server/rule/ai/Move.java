/*    */ package game.coup.server.rule.ai;
/*    */ 
/*    */ import game.coup.server.rule.GameResult;
/*    */ import game.coup.server.rule.GameResult.Name;
/*    */ import game.coup.server.rule.Piece;
/*    */ 
/*    */ public class Move
/*    */ {
/*    */   public static final int END_GAME = 1;
/*    */   public static final int CONTINUE = 2;
/*    */   public static final int ERROR_UNEXIST = 3;
/*    */   public static final int ERROR_INVALID = 4;
/*    */   public static final int ERROR_STATE = 4;
/*    */   public static final int CHIEU_CU_NHAY = 5;
/*    */   public static final int THE_HOA = 6;
/*    */   public int currentChair;
/*    */   public Piece piece;
/*    */   public int[] from;
/*    */   public int[] to;
/*    */   public Piece eatenPiece;
/* 21 */   public GameResult result = new GameResult();
/*    */   
/* 23 */   public boolean isTrans = false;
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */   public String toString()
/*    */   {
/* 30 */     StringBuilder sb = new StringBuilder();
/* 31 */     sb.append(this.piece).append(" move from {");
/* 32 */     sb.append(this.from[0]).append(",").append(this.from[1]).append("}");
/* 33 */     sb.append(" to {");
/* 34 */     sb.append(this.to[0]).append(",").append(this.to[1]).append("}");
/* 35 */     if (this.eatenPiece != null) {
/* 36 */       sb.append("eatenTransPiece ").append(this.eatenPiece);
/*    */     }
/* 38 */     sb.append(" result: ").append(this.result.result);
/* 39 */     sb.append(" isTrans: ").append(this.isTrans);
/* 40 */     return sb.toString();
/*    */   }
/*    */   
/*    */   public boolean sameWith(Move m) {
/* 44 */     return (m.piece.key == this.piece.key) && (((m.from[0] == this.from[0]) && (m.from[1] == this.from[1]) && (m.to[0] == this.to[0]) && (m.to[1] == this.to[1])) || ((m.from[0] == this.to[0]) && (m.from[1] == this.to[1]) && (m.to[0] == this.from[0]) && (m.to[1] == this.from[1])));
/*    */   }
/*    */   
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */   public void transform()
/*    */   {
/* 56 */     if (!this.piece.isTrans) {
/* 57 */       this.isTrans = true;
/* 58 */       this.piece.transform();
/*    */     }
/*    */   }
/*    */   
/*    */   public boolean isOK() {
/* 63 */     return (this.result.result == GameResult.Name.CONTINUE) || (this.result.result == GameResult.Name.DRAW) || (this.result.result == GameResult.Name.WIN_LOST);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\rule\ai\Move.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */