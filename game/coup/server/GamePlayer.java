/*     */ package game.coup.server;
/*     */ 
/*     */ import bitzero.server.entities.User;
/*     */ import game.entities.PlayerInfo;
/*     */ import game.modules.gameRoom.entities.GameMoneyInfo;
/*     */ import org.json.JSONObject;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class GamePlayer
/*     */ {
/*     */   public static final int psNO_LOGIN = 0;
/*     */   public static final int psVIEW = 1;
/*     */   public static final int psSIT = 2;
/*     */   public static final int psPLAY = 3;
/*     */   public static final int THANG_SAM = 1;
/*     */   public static final int THANG_TRANG = 2;
/*     */   public static final int THANG_THUONG = 3;
/*     */   public int chair;
/*  24 */   public int gameChair = -1;
/*  25 */   public long timeJoinRoom = 0L;
/*  26 */   public int countToOutRoom = 0;
/*  27 */   public boolean reqQuitRoom = false;
/*  28 */   public boolean regToPlay = false;
/*  29 */   public boolean regToView = false;
/*     */   
/*     */ 
/*  32 */   public boolean choiTiepVanSau = true;
/*  33 */   public User user = null;
/*  34 */   public PlayerInfo pInfo = null;
/*  35 */   public GameMoneyInfo gameMoneyInfo = null;
/*  36 */   public sPlayerInfo spInfo = new sPlayerInfo();
/*     */   public CoupGameServer gameServer;
/*  38 */   public boolean autoBuyIn = true;
/*  39 */   public volatile long lastTimeBuyIn = 0L;
/*  40 */   public int cauHoa = 0;
/*  41 */   public int thachDau = 0;
/*  42 */   public boolean dangCauHoa = false;
/*  43 */   public boolean dangThachDau = false;
/*     */   
/*     */ 
/*  46 */   public volatile int playerStatus = 0;
/*     */   
/*     */   public void prepareNewGame() {
/*  49 */     if (this.gameChair >= 0) {
/*  50 */       this.playerStatus = 2;
/*     */     }
/*  52 */     this.cauHoa = 0;
/*  53 */     this.dangCauHoa = false;
/*  54 */     this.thachDau = 0;
/*  55 */     this.dangThachDau = false;
/*     */   }
/*     */   
/*  58 */   public User getUser() { return this.user; }
/*     */   
/*     */   public PlayerInfo getPlayerInfo() {
/*  61 */     return this.pInfo;
/*     */   }
/*     */   
/*     */   public void takeChair(User user, PlayerInfo pInfo, GameMoneyInfo moneyInfo) {
/*  65 */     this.user = user;
/*  66 */     this.pInfo = pInfo;
/*  67 */     this.gameMoneyInfo = moneyInfo;
/*  68 */     this.reqQuitRoom = false;
/*  69 */     user.setProperty("user_chair", Integer.valueOf(this.chair));
/*     */   }
/*     */   
/*     */   public boolean isPlaying() {
/*  73 */     return this.playerStatus == 3;
/*     */   }
/*     */   
/*     */   public boolean canPlayNextGame() {
/*  77 */     return (!this.reqQuitRoom) && (checkMoneyCanPlay()) && (this.regToPlay);
/*     */   }
/*     */   
/*     */   public boolean hasUser() {
/*  81 */     return this.playerStatus != 0;
/*     */   }
/*     */   
/*     */   public boolean checkMoneyCanPlay() {
/*  85 */     return this.gameMoneyInfo.moneyCheck();
/*     */   }
/*     */   
/*     */   public String toString() {
/*     */     try {
/*  90 */       JSONObject json = toJSONObject();
/*  91 */       if (json != null) {
/*  92 */         return json.toString();
/*     */       }
/*  94 */       return "{}";
/*     */     }
/*     */     catch (Exception e) {}
/*  97 */     return "{}";
/*     */   }
/*     */   
/*     */   public JSONObject toJSONObject()
/*     */   {
/*     */     try {
/* 103 */       JSONObject json = new JSONObject();
/* 104 */       json.put("reqQuitRoom", this.reqQuitRoom);
/* 105 */       json.put("playerStatus", this.playerStatus);
/* 106 */       if (this.gameMoneyInfo != null) {
/* 107 */         json.put("gameMoneyInfo", this.gameMoneyInfo.toJSONObject());
/*     */       }
/* 109 */       return json;
/*     */     } catch (Exception e) {}
/* 111 */     return null;
/*     */   }
/*     */   
/*     */   public boolean updatePlayingTime()
/*     */   {
/* 116 */     this.spInfo.turnTime -= 1;
/* 117 */     this.spInfo.gameTime -= 1;
/* 118 */     if ((this.spInfo.turnTime <= 0) || (this.spInfo.gameTime <= 0)) {
/* 119 */       return false;
/*     */     }
/* 121 */     return true;
/*     */   }
/*     */   
/*     */   public boolean checkMoreMoneyThan(long money) {
/* 125 */     if (this.gameMoneyInfo != null) {
/* 126 */       return this.gameMoneyInfo.getCurrentMoneyFromCache() >= money;
/*     */     }
/* 128 */     return false;
/*     */   }
/*     */   
/*     */   public void outChair() {
/* 132 */     this.regToView = false;
/* 133 */     this.choiTiepVanSau = true;
/* 134 */     this.countToOutRoom = 0;
/* 135 */     this.gameChair = -1;
/* 136 */     this.cauHoa = 0;
/* 137 */     this.dangCauHoa = false;
/* 138 */     this.thachDau = 0;
/* 139 */     this.dangThachDau = false;
/*     */   }
/*     */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\GamePlayer.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */