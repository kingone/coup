/*    */ package game.coup.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import game.coup.server.GamePlayer;
/*    */ import game.entities.PlayerInfo;
/*    */ import game.modules.gameRoom.entities.GameMoneyInfo;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SendJoinRoomSuccess
/*    */   extends BaseMsg
/*    */ {
/*    */   public int uChair;
/*    */   public int comission;
/*    */   public int comissionJackpot;
/*    */   public long moneyBet;
/*    */   public byte roomOwner;
/*    */   public int gameId;
/*    */   public int roomId;
/*    */   public int moneyType;
/*    */   public int rule;
/* 27 */   public byte[] playerStatus = new byte[20];
/* 28 */   public GamePlayer[] playerList = new GamePlayer[20];
/*    */   public byte gameState;
/*    */   public byte gameAction;
/*    */   public byte curentChair;
/*    */   public byte countDownTime;
/* 33 */   public GameMoneyInfo[] moneyInfoList = new GameMoneyInfo[20];
/*    */   
/*    */   public SendJoinRoomSuccess() {
/* 36 */     super((short)3118);
/*    */   }
/*    */   
/*    */   public SendJoinRoomSuccess(int i) {
/* 40 */     super((short)3118, i);
/*    */   }
/*    */   
/*    */   public byte[] createData()
/*    */   {
/* 45 */     ByteBuffer bf = makeBuffer();
/* 46 */     bf.put((byte)this.uChair);
/* 47 */     bf.putLong(this.moneyBet);
/* 48 */     bf.put(this.roomOwner);
/* 49 */     bf.putInt(this.roomId);
/* 50 */     bf.putInt(this.gameId);
/* 51 */     bf.put((byte)this.moneyType);
/* 52 */     bf.put((byte)this.rule);
/* 53 */     putByteArray(bf, this.playerStatus);
/* 54 */     bf.putShort((short)this.playerList.length);
/* 55 */     for (int i = 0; i < this.playerList.length; i++) {
/* 56 */       GamePlayer gp = this.playerList[i];
/* 57 */       PlayerInfo pInfo = null;
/* 58 */       if (!gp.hasUser()) {
/* 59 */         pInfo = new PlayerInfo();
/* 60 */         bf.put((byte)-1);
/* 61 */         bf.put((byte)-1);
/*    */       } else {
/* 63 */         pInfo = gp.pInfo;
/* 64 */         bf.put((byte)this.playerList[i].gameChair);
/* 65 */         bf.put((byte)this.playerList[i].chair);
/*    */       }
/* 67 */       putStr(bf, pInfo.avatarUrl);
/* 68 */       putStr(bf, pInfo.nickName);
/* 69 */       if (this.moneyInfoList[i] == null) {
/* 70 */         bf.putLong(0L);
/*    */       } else {
/* 72 */         bf.putLong(this.moneyInfoList[i].currentMoney);
/*    */       }
/*    */     }
/* 75 */     bf.put(this.gameAction);
/* 76 */     bf.put(this.curentChair);
/* 77 */     bf.put(this.countDownTime);
/* 78 */     bf.put(this.gameState);
/* 79 */     return packBuffer(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\cmd\send\SendJoinRoomSuccess.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */