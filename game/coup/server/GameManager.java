/*     */ package game.coup.server;
/*     */ 
/*     */ import game.coup.server.cmd.send.SendUpdateAutoStart;
/*     */ import game.coup.server.logic.Gamble;
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ 
/*     */ public class GameManager
/*     */ {
/*     */   public static final int GS_NO_START = 0;
/*     */   public static final int GS_GAME_PLAYING = 1;
/*     */   public static final int GS_GAME_END = 3;
/*     */   public static final int NO_ACTION = -1;
/*     */   public static final int IN_TURN = 0;
/*     */   public static final int CHANGE_TURN = 1;
/*  20 */   public int roomOwnerChair = 20;
/*     */   public int roomCreatorUserId;
/*  22 */   public int rCuocLon = 1;
/*     */   
/*     */ 
/*  25 */   public volatile int gameState = 0;
/*  26 */   public volatile int gameAction = -1;
/*     */   
/*     */ 
/*  29 */   public volatile int countDown = 0;
/*  30 */   public volatile boolean isAutoStart = false;
/*     */   
/*  32 */   public volatile int currentChair = -1;
/*  33 */   public volatile int lastWinChair = -1;
/*     */   public Gamble game;
/*     */   public CoupGameServer gameServer;
/*     */   
/*     */   public GameManager()
/*     */   {
/*  39 */     this.game = new Gamble();
/*     */   }
/*     */   
/*  42 */   public int getGameState() { return this.gameState; }
/*     */   
/*     */   public void prepareNewGame() {
/*  45 */     this.game.reset();
/*  46 */     this.isAutoStart = false;
/*  47 */     this.gameServer.kiemTraTuDongBatDau(5);
/*     */   }
/*     */   
/*     */   public void gameLoop() {
/*  51 */     if ((this.gameState == 0) && (this.isAutoStart)) {
/*  52 */       this.countDown -= 1;
/*  53 */       if (this.countDown <= 0) {
/*  54 */         this.gameState = 1;
/*  55 */         this.gameServer.start();
/*     */       }
/*     */     }
/*  58 */     else if ((this.gameState == 0) && (!this.isAutoStart)) {
/*  59 */       if (this.gameServer.playerCount > 1) {
/*  60 */         this.gameServer.kiemTraTuDongBatDau(5);
/*     */       }
/*     */     }
/*  63 */     else if (this.gameState == 1) {
/*  64 */       this.countDown -= 1;
/*  65 */       if (this.countDown <= 0) {
/*  66 */         if (this.gameAction == 1) {
/*  67 */           changeTurn();
/*     */         }
/*  69 */         else if (this.gameAction == 0) {
/*  70 */           tudongChoi();
/*     */         }
/*     */       }
/*  73 */       updatePlayingTime();
/*  74 */     } else if (this.gameState == 3) {
/*  75 */       this.countDown -= 1;
/*  76 */       if (this.countDown <= 0) {
/*  77 */         this.gameServer.pPrepareNewGame();
/*     */       }
/*     */     }
/*     */   }
/*     */   
/*  82 */   public void updatePlayingTime() { this.gameServer.updatePlayingTime(); }
/*     */   
/*     */   public void notifyAutoStartToUsers(int after) {
/*  85 */     SendUpdateAutoStart msg = new SendUpdateAutoStart();
/*  86 */     msg.isAutoStart = this.isAutoStart;
/*  87 */     msg.autoStartTime = ((byte)after);
/*  88 */     this.gameServer.send(msg);
/*     */   }
/*     */   
/*  91 */   public void cancelAutoStart() { if (this.isAutoStart) {
/*  92 */       this.isAutoStart = false;
/*  93 */       notifyAutoStartToUsers(0);
/*     */     }
/*     */   }
/*     */   
/*  97 */   public void makeAutoStart(int after) { if (this.gameState != 0) return;
/*  98 */     if (!this.isAutoStart)
/*     */     {
/* 100 */       this.countDown = after;
/*     */ 
/*     */ 
/*     */     }
/* 104 */     else if (after < this.countDown) {
/* 105 */       this.countDown = after;
/*     */     } else {
/* 107 */       after = this.countDown;
/*     */     }
/*     */     
/* 110 */     this.isAutoStart = true;
/* 111 */     notifyAutoStartToUsers(after);
/*     */   }
/*     */   
/* 114 */   private void tudongChoi() { this.gameServer.tudongChoi(); }
/*     */   
/*     */   private void changeTurn() {
/* 117 */     this.gameServer.changeTurn();
/*     */   }
/*     */   
/* 120 */   public int currentChair() { return this.currentChair; }
/*     */   
/*     */   public boolean canOutRoom()
/*     */   {
/* 124 */     return getGameState() == 0;
/*     */   }
/*     */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\GameManager.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */