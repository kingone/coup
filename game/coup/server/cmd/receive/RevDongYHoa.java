/*    */ package game.coup.server.cmd.receive;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseCmd;
/*    */ import bitzero.server.extensions.data.DataCmd;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ public class RevDongYHoa extends BaseCmd
/*    */ {
/*  9 */   public boolean dongYHoa = false;
/*    */   
/* 11 */   public RevDongYHoa(DataCmd data) { super(data);
/* 12 */     unpackData();
/*    */   }
/*    */   
/*    */   public void unpackData() {
/* 16 */     ByteBuffer bf = makeBuffer();
/* 17 */     this.dongYHoa = readBoolean(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\cmd\receive\RevDongYHoa.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */