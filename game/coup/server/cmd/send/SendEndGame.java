/*    */ package game.coup.server.cmd.send;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseMsg;
/*    */ import java.nio.ByteBuffer;
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ 
/*    */ public class SendEndGame
/*    */   extends BaseMsg
/*    */ {
/*    */   public byte result;
/*    */   public byte winChair;
/*    */   public long moneyWin;
/*    */   public long moneyLost;
/*    */   public int countdown;
/* 19 */   public long[] moneyArray = new long[2];
/*    */   
/* 21 */   public SendEndGame() { super((short)3103); }
/*    */   
/*    */   public byte[] createData()
/*    */   {
/* 25 */     ByteBuffer bf = makeBuffer();
/* 26 */     bf.put(this.result);
/* 27 */     bf.put(this.winChair);
/* 28 */     bf.putLong(this.moneyWin);
/* 29 */     bf.putLong(this.moneyLost);
/* 30 */     bf.put((byte)this.countdown);
/* 31 */     putLongArray(bf, this.moneyArray);
/* 32 */     return packBuffer(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\cmd\send\SendEndGame.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */