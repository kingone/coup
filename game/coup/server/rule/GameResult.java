/*    */ package game.coup.server.rule;
/*    */ 
/*    */ public class GameResult {
/*    */   public Name result;
/*    */   
/*  6 */   public static enum Name { WIN_LOST, 
/*  7 */     DRAW, 
/*  8 */     CONTINUE, 
/*  9 */     ERROR_UNEXIST, 
/* 10 */     ERROR_INVALID, 
/* 11 */     ERROR_STATE, 
/* 12 */     TIME_OUT, 
/* 13 */     RESIGN;
/*    */     
/*    */     private Name() {}
/*    */   }
/*    */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\rule\GameResult.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */