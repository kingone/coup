/*    */ package game.coup.server.cmd.receive;
/*    */ 
/*    */ import bitzero.server.extensions.data.BaseCmd;
/*    */ import bitzero.server.extensions.data.DataCmd;
/*    */ 
/*    */ public class RevTakeTurn extends BaseCmd
/*    */ {
/*    */   public byte[] from;
/*    */   public byte[] to;
/*    */   
/*    */   public RevTakeTurn(DataCmd dataCmd)
/*    */   {
/* 13 */     super(dataCmd);
/* 14 */     unpackData();
/*    */   }
/*    */   
/*    */   public void unpackData()
/*    */   {
/* 19 */     java.nio.ByteBuffer bf = makeBuffer();
/* 20 */     this.from = readByteArray(bf);
/* 21 */     this.to = readByteArray(bf);
/*    */   }
/*    */ }


/* Location:              C:\gamebai\coup\Coup.jar!\game\coup\server\cmd\receive\RevTakeTurn.class
 * Java compiler version: 8 (52.0)
 * JD-Core Version:       0.7.1
 */